Author: Kaixin Wang

Environment Prerequisites: Browser that supports css, javascript and html, Bash Environment

Instructions on how to build and run the project 
1. Clone the repository into your local storage by using the git command below: git clone https://kxinnnn@bitbucket.org/kxinnnn/ui-chatbot-ghost-story.git
2. open the folder created and open cmd commend prompt within the folder directory
3. Run npm install to install relevant dependencies that are required to run the app.
4. Run node index.js to start up the localhost.
5. In browser type localhost:XXXXXX ->  XXXXX is the port number which is given in the command prompt after node index.js. 

You can also visit the live site for this project on https://prog8110assignment1kaixinwang.herokuapp.com/

Explaination of choice of License: The MIT License(https://opensource.org/licenses/MIT) is permissive as the modified versions does not have to be issued under the same licensence, files does not have to contain all information about changes, copyrights and patents and it does not prohibit the use of copyright holder's name for promotion. The persmissiveness would allow me to combine codes with other open source libraries with only few restrictions. The license is also simple unlike many other licenses with long and complex text.